﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Models;

namespace WebApplication1.Services
{
    public class UserService : IUserService
    {
        public IServiceProvider serviceProvider { get; }

        public UserService(IServiceProvider _serviceProvider)
        {
            serviceProvider = _serviceProvider;
        }

        public async Task CreateUser(ApplicationUser user, string role)
        {
            var roleManager = (RoleManager<IdentityRole>) serviceProvider.GetService(typeof(RoleManager<IdentityRole>));
            var userManager = (UserManager<ApplicationUser>) serviceProvider.GetService(typeof(UserManager<ApplicationUser>));

            ApplicationUser testUser = await userManager.FindByEmailAsync(user.Email);
            
            if (testUser == null)
            {
                IdentityResult newUser = await userManager.CreateAsync(user);

                if (newUser.Succeeded)
                {
                    switch (role)
                    {
                        case "Administrator":
                            await userManager.AddToRoleAsync(user, "Administrator");
                            await userManager.AddToRoleAsync(user, "Seller");
                            await userManager.AddToRoleAsync(user, "Customer");
                            break;

                        case "Seller":
                            await userManager.AddToRoleAsync(user, "Seller");
                            await userManager.AddToRoleAsync(user, "Customer");
                            break;

                        case "Customer":
                            await userManager.AddToRoleAsync(user, "Customer");
                            break;
                    }
                }                
            }
        }
    }
}
