﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebApplication1.Models.Catalog;

namespace WebApplication1.Models
{
    public class ProductContext : DbContext
    {
        public ProductContext (DbContextOptions<ProductContext> options)
            : base(options)
        {
        }

        public DbSet<WebApplication1.Models.Catalog.Product> Product { get; set; }

        public DbSet<WebApplication1.Models.Catalog.CartItem> CartItem { get; set; }
    }
}
